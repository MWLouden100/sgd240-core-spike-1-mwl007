# Spike Report

## SPIKE TITLE - Development Environment

### Introduction

This spike report details how to set up a development environment for Unreal Engine, C++, and Git using SourceTree software.
This knowledge is required to create an appropriate setup to support later works.

### Goals

1. Learn how to create Unreal Engine projects & Unreal Engine C++ projects
2. Enhance understanding of git systems and how they work in concert with Unreal Engine
3. Learn how to use formal workflows like GitFlow

### Personnel

* Primary - Matthew Louden
* Secondary - N/A

### Technologies, Tools, and Resources used

* [.Gitignore File Script](https://github.com/github/gitignore/blob/master/UnrealEngine.gitignore)

### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Generated a new Repository on Github
	1. [Cloned Repository to Sourcetree] (https://confluence.atlassian.com/sourcetreekb/clone-a-repository-into-sourcetree-780870050.html)
	1. [Added a .gitignore file to the repository] (https://github.com/github/gitignore/blob/master/UnrealEngine.gitignore)
		1b. (done by copy pasting .gitignore code into a .txt file then changing the extention to .gitignore)
2. Created a new Unreal Engine 4 project
	2. Added UE4 project to repository
3. [Committed and pushed repository on sourcetree] (https://support.atlassian.com/bitbucket-cloud/docs/tutorial-learn-bitbucket-with-sourcetree/)

### What we found out

We learnt how to create unreal engine projects and enhanced our understanding of git systems and repository systems.

